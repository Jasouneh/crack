#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[] ) {
    FILE *fp;
    FILE *fp2;
    
    char word[100];
    fp = fopen(argv[1], "r");
    fp2 = fopen(argv[2], "w");
    
    if ( fp ) {
        while ( fgets( word, 100, fp) != NULL ) {

            char *hash = md5( word, strlen(word)-1);
            fprintf(fp2, "%s\n", hash);
            
        }
        
    }
    else {
        printf("File not found");
    }
    
    fclose(fp);
    fclose(fp2);
}